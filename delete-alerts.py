#!/usr/bin/env python3

import argparse
import logging

from http.client import HTTPConnection

import requests

def arg_parser():
    parser = argparse.ArgumentParser(
        description='Delete alerts from Zoopla with ids found in `alert-ids.txt` file.')
    parser.add_argument('zooplapsid', help='Zoopla `zooplapsid` cookie value')
    parser.add_argument('zooplasid', help='Zoopla `zooplasid` cookie value')

    return parser

def main():
    parser = arg_parser()
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)
    HTTPConnection.debuglevel = 1

    cookies = {
        'zooplapsid': args.zooplapsid,
        'zooplasid': args.zooplasid
    }
    with open('alert-ids.txt', 'r') as alert_ids:
        ids = {l.strip() for l in alert_ids.readlines()}
    for alert_id in ids:
        print(f'processing: {alert_id}')
        res = requests.get(f'https://www.zoopla.co.uk/myaccount/alerts-searches/delete/{alert_id}',
                           params={'section': 'for-sale'},
                           cookies=cookies, allow_redirects=False)
        res.raise_for_status()

if __name__ == '__main__':
    main()
