#!/usr/bin/env python3

import argparse
import logging

from http.client import HTTPConnection

import requests

from bs4 import BeautifulSoup

def arg_parser():
    parser = argparse.ArgumentParser(description='List alerts that can be found on Zoopla')
    parser.add_argument('zooplapsid', help='Zoopla `zooplapsid` cookie value')
    parser.add_argument('zooplasid', help='Zoopla `zooplasid` cookie value')

    return parser

def main():
    parser = arg_parser()
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)
    HTTPConnection.debuglevel = 1

    cookies = {
        'zooplapsid': args.zooplapsid,
        'zooplasid': args.zooplasid
    }
    res = requests.get('https://www.zoopla.co.uk/myaccount/alerts-searches/', cookies=cookies)
    res.raise_for_status()
    soup = BeautifulSoup(res.content, 'html.parser')
    alerts = soup.select("#tab-residential-for-sale input[name='description']")
    print(f'found #{len(alerts)} alerts')
    with open('alerts.txt', 'w') as alerts_file:
        for alert in alerts:
            alerts_file.write(alert['value'] + '\n')

if __name__ == '__main__':
    main()
