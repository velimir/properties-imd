#!/usr/bin/env python3

import csv
import xml.sax

from polylabel import polylabel

class LLSOAHandler(xml.sax.ContentHandler):

    def __init__(self):
        self._code = None
        self._lsoas = {}
        self._sdata_name = None
        self._ctag = None
        self._coordinates = ''
        super().__init__()

    @property
    def lsoas(self):
        return self._lsoas

    def startElement(self, tag, attributes): # pylint: disable=arguments-differ
        if tag == 'SimpleData' and attributes['name'] == 'lsoa11cd':
            self._ctag = 'SimpleData'
            self._sdata_name = attributes['name']
            self._code = ''
        elif tag == 'coordinates':
            self._ctag = 'coordinates'

    def characters(self, content):
        if self._ctag == 'SimpleData' and self._sdata_name == 'lsoa11cd':
            self._code += content
        elif self._ctag == 'coordinates':
            self._coordinates += content

    def endElement(self, tag): # pylint: disable=arguments-differ
        if tag == 'coordinates':
            points = [(float(lng), float(lat))
                      for lng, lat in [p.split(',')
                                       for p in self._coordinates.split()]]
            self._lsoas[self._code] = {
                'label': polylabel([points]),
                'coordinates': self._coordinates
            }
            self._coordinates = ''
        self._ctag = ''

def dump_to_csv(data):
    with open('LLSOA-labels.tsv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['LSOAC', 'label', 'coordinates'])
        for code, info in data.items():
            label = ','.join(map(str, info['label']))
            coords = info['coordinates']
            writer.writerow([code, label, coords])

def main():
    parser = xml.sax.make_parser()
    handler = LLSOAHandler()
    parser.setContentHandler(handler)
    parser.parse("LLSOA.kml")

    dump_to_csv(handler.lsoas)

if __name__ == '__main__':
    main()
