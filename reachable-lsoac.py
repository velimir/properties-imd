#!/usr/bin/env python3

import argparse

from collections import namedtuple

import requests

import pandas as pd

Location = namedtuple('Location', ['lat', 'lng', 'id'])

STATIONS = [
    Location(51.503139, -0.112273, "Waterloo"),
    Location(51.516734, -0.176734, "Paddington"),
    Location(51.531959, -0.123276, "King's Cross"),
    Location(51.531434, -0.126109, "St Pancras"),
    Location(51.528877, -0.134303, "Euston"),
    Location(51.508174, -0.124746, "Charing Cross"),
    Location(51.494625, -0.143813, "Victoria St"),
    Location(51.504286, -0.084666, "London Bridge"),
    Location(51.511575, -0.078232, "London Fenchurch Street"),
    Location(51.518865, -0.081298, "Liverpool Street")
]

def arg_parser():
    parser = argparse.ArgumentParser(
        description='Fileter out areas that are not within reachable distance.',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('application_id', help='traveltimeapp.com application id')
    parser.add_argument('api_key', help='traveltimeapp.com api key')
    parser.add_argument('--travel-time', type=int, default=50,
                        help='Maximum travel time door to door on a public transport door to door in minutes. (default: %(default)s)')


    return parser

def main():
    # pylint: disable=too-many-locals

    parser = arg_parser()
    args = parser.parse_args()

    labels = pd.read_csv('top-lsoac-loc.csv', index_col=0)

    stations_locations = [
        {
            'coords': {
                'lat': loc.lat,
                'lng': loc.lng
            },
            'id': loc.id
        }
        for loc in STATIONS
    ]

    locations = stations_locations.copy()

    for lsoac in labels.itertuples():
        lng, lat = lsoac.label.split(',')
        locations.append({
            'coords': {
                'lat': float(lat),
                'lng': float(lng)
            },
            'id': lsoac.Index
        })

    searches = {
        'many_to_one': [
            {
                'id': station['id'],
                'departure_location_ids': [loc['id'] for loc in locations],
                'arrival_location_id': station['id'],
                'transportation': {
                    'type': 'public_transport'
                },
                'arrival_time_period': 'weekday_morning',
                'travel_time': args.travel_time * 60,
                'properties': [
                    'travel_time',
                    'fares'
                ]
            }
            for station in stations_locations
        ]
    }

    res = requests.post(
        'https://api.traveltimeapp.com/v4/time-filter/fast',
        headers={'X-Application-Id': args.application_id,
                 'X-Api-Key': args.api_key},
        json={'locations': locations, 'arrival_searches': searches})
    searches = res.json()['results']

    def price(fares, transportation_type):
        fare = next(filter(lambda it: it['type'] == transportation_type, fares), {})
        return fare.get('price', 0)

    travel = pd.DataFrame(index=[], columns=['travel_time', 'fare_price_month', 'fare_price_year'])
    travel.index.name = labels.index.name

    for results in searches:
        for location in results['locations']:
            code = location['id']
            props = location['properties']
            fares = props['fares']['tickets_total']
            travel.loc[code] = {
                'travel_time': props['travel_time'],
                'fare_price_month': price(fares, 'month'),
                'fare_price_year': price(fares, 'year')
            }

    reachable_lsoac = pd.merge(labels, travel, left_index=True, right_index=True)
    reachable_lsoac.to_csv('reachable-lsoac.csv')

if __name__ == '__main__':
    main()
