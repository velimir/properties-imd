#!/usr/bin/env python3

import argparse
import logging

from http.client import HTTPConnection

import polyline
import requests

import pandas as pd

def arg_parser():
    parser = argparse.ArgumentParser(
        description=('Import LSOAC (lower layer super output area codes) into zoopla. '
                     'Script read `alerts.txt` file and skip codes that can be found in the file.'),
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('zooplapsid', help='Zoopla `zooplapsid` cookie value')
    parser.add_argument('zooplasid', help='Zoopla `zooplasid` cookie value')

    return parser

def create_alert(lsoan, polyenc, zooplapsid, zooplasid):
    request = {
        'description': lsoan,
        'frequency': '1',
        'user_alert_id': '',
        'price_min': '',
        'price_max': '500000',
        'section': 'for-sale',
        'q': 'London',
        'identifier': 'london',
        'radius': '0',
        'property_type': 'houses',
        'beds_min': '2',
        'beds_max': '',
        'keywords': '',
        'added': '',
        'view_type': 'list',
        'page_size': '25',
        'category': 'residential',
        'new_homes': 'include',
        'include_shared_ownership': '',
        'chain_free': '',
        'reduced_price_only': '',
        'is_shared_ownership': 'false',
        'buyer_incentive': '',
        'include_retirement_homes': '',
        'include_sold': '',
        'is_retirement_home': 'false',
        'is_auction': '',
        'property_sub_type': '',
        'feature': '',
        'duration': '',
        'transport_type': '',
        'floor_area_min': '',
        'floor_area_max': '',
        'floor_area_units': '',
        'skip_save': '0',
        'pref': 'alertsres',
        'polyenc': polyenc,
        'action:save': 'Save search'
    }

    cookies = {
        'zooplapsid': zooplapsid,
        'zooplasid': zooplasid
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Safari/605.1.15',
        'Referer': 'https://www.zoopla.co.uk/',
        'Origin': 'https://www.zoopla.co.uk',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-gb',
        'Accept': '*/*'
    }
    url = 'https://www.zoopla.co.uk/ajax/myaccount/alerts_searches'
    res = requests.post(url, data=request, cookies=cookies, headers=headers)
    res.raise_for_status()

def main():
    parser = arg_parser()
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)
    HTTPConnection.debuglevel = 1

    with open('alerts.txt', 'r') as existing_alerts:
        ignore_lsoas = {l.strip() for l in existing_alerts.readlines()}

    df = pd.read_csv('reachable-lsoac-filtered.csv', index_col=0)
    for lsoa in df.itertuples():
        if lsoa.LSOAN in ignore_lsoas:
            print(f'skipping {lsoa.LSOAN}')
            continue

        points = [(float(lat), float(lng)) for lng, lat in [p.split(',') for p in lsoa.coordinates.split()]]
        polyenc = polyline.encode(points, 5)
        print(f'processing: {lsoa.LSOAN}')
        create_alert(lsoa.LSOAN, polyenc, args.zooplapsid, args.zooplasid)

if __name__ == '__main__':
    main()
