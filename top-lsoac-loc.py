#!/usr/bin/env python3

import pandas as pd

def read_csv():
    return pd.read_csv(
        'IMD2019.tsv',
        sep='\t',
        header=0,
        names=['LSOAC', 'LSOAN', 'LADC', 'LADN', 'IMD', 'IMDD'],
        converters={'IMD': lambda x: int(x.replace(',', ''))},
        index_col=0)

def main():
    df = read_csv()
    top_lsoac = df[df.IMD > df.IMD.quantile(.8)].copy()
    labels = pd.read_csv('LLSOA-labels.tsv', sep='\t', index_col=0)
    top_lsoac_loc = pd.merge(top_lsoac, labels, left_index=True, right_index=True)
    top_lsoac_loc.to_csv('top-lsoac-loc.csv')

if __name__ == '__main__':
    main()
